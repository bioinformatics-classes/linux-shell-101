### Linux shell 101

![Linux](assets/linux.jpg)

© Francisco Pina Martins 2015-2025

---

### What is the shell?

* &shy;<!-- .element: class="fragment" -->A "command line" interface
* &shy;<!-- .element: class="fragment" --> *bash* (*ksh* *zsh* *fish* *dash* etc...)
* &shy;<!-- .element: class="fragment" --> Can be accessed via a "terminal"
* &shy;<!-- .element: class="fragment" --> **Mostly** keyboard driven
* &shy;<!-- .element: class="fragment" --> The "mouse cursor" is also useful here

---

### Why do I need it?

* &shy;<!-- .element: class="fragment" -->HPC clusters
* &shy;<!-- .element: class="fragment" -->Remote access
* &shy;<!-- .element: class="fragment" -->**Simple** interface
* &shy;<!-- .element: class="fragment" -->Process automation & reproducibility

---

## The *prompt*

``user@machine:~$``

``bash-4.1$``

![Prompt image](assets/shell_prompt_small.gif)

[Pimp my prompt](https://www.cyberciti.biz/tips/howto-linux-unix-bash-shell-setup-prompt.html) <!-- .element: class="fragment" data-fragment-index="1" -->

---

### Navigation & orientation

<div style="float: right"><img src="assets/shell_prompt_small.gif" /></div>

* &shy;<!-- .element: class="fragment" -->Where am I?
  * &shy;<!-- .element: class="fragment" -->`$ pwd` 
  * &shy;<!-- .element: class="fragment" --><font color="red">p</font>rint <font color="red">w</font>orking <font color="red">d</font>irectory 
* &shy;<!-- .element: class="fragment" -->What is in here? 
  * &shy;<!-- .element: class="fragment" -->``$ ls`` 
  * &shy;<!-- .element: class="fragment" --><font color="red">l</font>i<font color="red">s</font>t 
* &shy;<!-- .element: class="fragment" -->How do I move around?
  * &shy;<!-- .element: class="fragment" -->``$ cd`` 
  * &shy;<!-- .element: class="fragment" --><font color="red">c</font>all <font color="red">d</font>irectory 

---

### Your new best freinds

* &shy;<!-- .element: class="fragment" -->``man program_name``
* &shy;<!-- .element: class="fragment" -->**EVERY** shell **core** program has it
* &shy;<!-- .element: class="fragment" -->Some others have it too

</br>

<div class="fragment" style="float: right"><img src="assets/shell_prompt_small.gif" /></div>

</br>

* &shy;<!-- .element: class="fragment" -->``$ man ls``
* &shy;<!-- .element: class="fragment" -->``$ whatis ls``

---

### Useful keyboard navigation keys

<div class="fragment" style="float: right"><img src="assets/Tab-Key.png" /></div>

* "Tab" Key
  * &shy;<!-- .element: class="fragment" -->Completes your command
  * &shy;<!-- .element: class="fragment" -->Try with one or two "*keypresses*"
* &shy;<!-- .element: class="fragment" -->↓ and ↑ keys
  * &shy;<!-- .element: class="fragment" -->Navigate previous commands

|||

### Useful keyboard navigation keys

```bash
cd  # Moves back to ~/

ls  # Lists current directory (~/) contents

ls Down<TAB>  # Completes the word "Downloads"
ls D<TAB><TAB>  # Shows all options starting with "D"
↑  # Pressing the up arrow reveals the previously entered command
```

---

### "The PATH"

* &shy;<!-- .element: class="fragment" -->Paths can be absolute...
  * &shy;<!-- .element: class="fragment" -->`/etc/fstab`
  * &shy;<!-- .element: class="fragment" -->`~/Downloads/ficheiro.txt`
* &shy;<!-- .element: class="fragment" -->Or relative...
  * &shy;<!-- .element: class="fragment" -->`Downloads/ficheiro.txt`
  * &shy;<!-- .element: class="fragment" -->`./ficheiro.txt`
* &shy;<!-- .element: class="fragment" --> Frequent convention examples
  * &shy;<!-- .element: class="fragment" --><font color="red">~</font> -> *home dir*, ex. `/home/francisco`
  * &shy;<!-- .element: class="fragment" --><font color="red">./</font> -> "Here"
  * &shy;<!-- .element: class="fragment" --><font color="red">../</font> -> One directory below
  * &shy;<!-- .element: class="fragment" --><font color="red">../../</font> -> Two 'dirs' below
  * &shy;<!-- .element: class="fragment" --><font color="red">.file</font> -> "Hidden" file

|||

### The PATH

```bash
cd
ls /etc/fstab  # Lists a single file
ls ~/  # Lists contents of your home dir
touch ./aa  # Create a new file (or update date if already exists)
cd Downloads
ls ../
touch .aa
ls -a  # Now you see it
ls  # Now you don't
```

---

### Navigation part II

1. How do I create a new dir (AKA - folder)? <!-- .element: class="fragment" data-fragment-index="1" -->
2. How do I erase a dir? <!-- .element: class="fragment" data-fragment-index="2" -->
3. How do I create a new file? <!-- .element: class="fragment" data-fragment-index="3" -->
4. How do I copy a file? <!-- .element: class="fragment" data-fragment-index="4" -->
5. How do I move a file? <!-- .element: class="fragment" data-fragment-index="5" -->
6. How do I erase a file? <!-- .element: class="fragment" data-fragment-index="6" -->

<div style="float: right"><img src="assets/shell_prompt_small.gif" /></div>

|||

### Navigation part II

```bash
mkdir dir_name
rmdir dir_name  # Dir needs to be empty!
touch file_name
cp origin destination
mv origin destination
rm file_name
```

<div class="fragment" style="color:red;">

Be **very**, **very** careful with `rm`

</div>

---

## Take 2'

---

## Permissions

#### Every file has "attributes"

```bash 
ls -l
drwxr-xr-x  5 francisco francisco 4096 Oct 22 00:24 Desktop
drwxr-xr-x  3 francisco cobig2    4096 2013-05-20 13:55 Databases
-rw-r--r--  1 francisco francisco 4256 Sep 15  2011 Zkill.py
```

* &shy;<!-- .element: class="fragment" -->3 scopes
  * &shy;<!-- .element: class="fragment" -->user
  * &shy;<!-- .element: class="fragment" -->group
  * &shy;<!-- .element: class="fragment" -->others
* &shy;<!-- .element: class="fragment" -->Each of which has 3 "values"
  * &shy;<!-- .element: class="fragment" --><font color="red">r</font>ead (4)
  * &shy;<!-- .element: class="fragment" --><font color="red">w</font>rite (2)
  * &shy;<!-- .element: class="fragment" -->e<font color="red">x</font>ecute (1)

---

### Permissions Part II

* &shy;<!-- .element: class="fragment" -->Changing ownership is simple:
  * &shy;<!-- .element: class="fragment" -->`$ chown user:group file`
* &shy;<!-- .element: class="fragment" -->Changing permissions... not so much:
  * &shy;<!-- .element: class="fragment" -->`chmod mode file`
    * &shy;<!-- .element: class="fragment" -->5 - read&execute (4+1)
    * &shy;<!-- .element: class="fragment" -->6 - read&write (4+2)
    * &shy;<!-- .element: class="fragment" -->7 - read&write&execute (4+2+1)
  * &shy;<!-- .element: class="fragment" -->Using trios of <font color="green">U</font><font color="blue">G</font><font color="red">O</font> (<font color="green">U</font>ser<font color="blue">G</font>roup<font color="red">O</font>thers)
    * &shy;<!-- .element: class="fragment" --><font color="green">7</font><font color="blue">7</font><font color="red">7</font> or <font color="green">7</font><font color="blue">5</font><font color="red">5</font> or <font color="green">6</font><font color="blue">4</font><font color="red">0</font>
  * &shy;<!-- .element: class="fragment" -->Or using a "verbal" form
    * &shy;<!-- .element: class="fragment" -->`chmod +x file`
    * &shy;<!-- .element: class="fragment" -->`chmod g-w file`

|||

### Permissions Part II

Try it:

```bash
groups  # Shows which groups your user belongs to
touch perm_test_file  # Create a new empty file for testing
# chmod and chown at will
```

---

### Environmental variables

Dynamically named values that can be "recalled"

```bash
test_var="hello"  # Defines the variable $test_var
echo $test_var  # What does `echo` do?
echo $USER  # Pre-defined variables
echo $HOME
# aliases are very similar:
alias ll="ls -l"
ll
alias hi="echo 'Hello there'"  # Notice the use of different quotation marks
```

<p class="fragment">Any env var or alias you set will be "forgotten" the moment you close your shell session.</p>

---

### Whitespace in the shell

* &shy;<!-- .element: class="fragment" -->A whitespace character separates a program from its arguments
    * &shy;<!-- .element: class="fragment" -->`ls -l`
* &shy;<!-- .element: class="fragment" -->Frequently, more than one argument is "passed" to the program
    * &shy;<!-- .element: class="fragment" -->`cp origin destination`
* &shy;<!-- .element: class="fragment" -->What if a filename contains spaces?
    * &shy;<!-- .element: class="fragment" -->"Escape" it with a backslash:
        * &shy;<!-- .element: class="fragment" -->`cat filename\ with\ spaces.txt`
    * &shy;<!-- .element: class="fragment" -->Place it within quotes:
        * &shy;<!-- .element: class="fragment" -->`cat 'filename with spaces.txt'`

---

### Awesome starts here

#### ``wget/curl grep sed cat less vim nano git``

Remember your [homework](assets/homework.txt)?

```bash
wget https://gitlab.com/bioinformatics-classes/linux-shell-101/raw/master/assets/homework.txt  # See also `curl`
cat homework.txt
grep ">" homework.txt
sed 's/>/+/g' homework.txt
less homework.txt  # Press 'q' to exit less and return to the shell prompt
nano homework.txt
```

&shy;<!-- .element: class="fragment" -->[Only the brave will be able to use Vim...](https://vim-adventures.com/)

&shy;<!-- .element: class="fragment" -->[(...or the smart)](http://www.vimgenius.com/start)

&shy;<!-- .element: class="fragment" -->We will return to `git` [another day](https://www.youtube.com/watch?v=iMfgcA1zlGY)

---

### *Wildcards*

* &shy;<!-- .element: class="fragment" -->Special characters that mean something other than their "literal"
  * &shy;<!-- .element: class="fragment" -->"\*" - any character any number of times
  * &shy;<!-- .element: class="fragment" -->"?" - any character, once

|||

### *Wildcards*

```bash
ls D*
cp *.txt some/other/dir
```
---

### Unix pipe: "|"

#### Command output can be used as input for a following program

```bash
groups
groups | tr " " "\n"  # What is happening here?
groups | tr " " "\n" | sort  # What about here?
```

&shy;<!-- .element: class="fragment" -->![Unlimited power](assets/unlimited.gif)

---

### *Redirects*

* &shy;<!-- .element: class="fragment" -->Represented by the `>` character
* &shy;<!-- .element: class="fragment" -->Very similar to `|`, but *redirects* a program's output into a file instead of another program
* &shy;<!-- .element: class="fragment" -->Can be used in may ways: 

<div class="fragment">

```bash
ls -l > file.txt  # resets a file to 0 bytes and writes to it
ls -l >> file.txt  # appends text to the end of the file
```

</div>

---

### Combos

```bash
cat homework.txt | grep ">"  > sequence_titles.txt
# Even better:
cat homework.txt |grep ">" | sed 's/>//g' > sequence_names.txt
# Who can figure this one out?
```

---

### How do redirects work?

* &shy;<!-- .element: class="fragment" -->`stdin`
  * &shy;<!-- .element: class="fragment" -->`0<` or `<`
* &shy;<!-- .element: class="fragment" -->`stdout`
  * &shy;<!-- .element: class="fragment" -->`>1` or `>`
* &shy;<!-- .element: class="fragment" -->`stderr`
  * &shy;<!-- .element: class="fragment" -->`>2`
* &shy;<!-- .element: class="fragment" -->Be **very** careful when using ">" in shell commands

|||

### How do redirects work?

```bash
tr "T" "U" < homework.txt
tr "T" "U" < homework.txt > homework_RNA.txt
python3 homework.txt
python3 homework.txt > nope.txt
python3 homework.txt 2> yup.txt
# Check the contents of these 2 files and tell me what happened
```

---

### (De)compressing files

<div style="float: right"><img src="assets/shell_prompt_small.gif" /></div>

* &shy;<!-- .element: class="fragment" -->Zip files
  * &shy;<!-- .element: class="fragment" -->`unzip file.zip`
* &shy;<!-- .element: class="fragment" -->"tar" files
  * &shy;<!-- .element: class="fragment" -->`tar xvfz file.tar.gz`
  * &shy;<!-- .element: class="fragment" -->`tar xvfj file.tar.bz2`
  * &shy;<!-- .element: class="fragment" -->`tar xvfJ file.tar.xz`
* &shy;<!-- .element: class="fragment" -->We can also use `tar` to compress
  * &shy;<!-- .element: class="fragment" -->`tar cvfJ file.tar.xz dir_to_compress`
* &shy;<!-- .element: class="fragment" -->We can use `gzip` to compress directly from `stdin`
  * &shy;<!-- .element: class="fragment" -->`cat file.txt | gzip > file.txt.gz`
  * &shy;<!-- .element: class="fragment" -->(Use `gunzip` to uncompress)

|||

### Try this:

```bash
wget https://gitlab.com/bioinformatics-classes/linux-shell-101/raw/master/assets/file.tar.gz
wget https://gitlab.com/bioinformatics-classes/linux-shell-101/raw/master/assets/file.tar.bz2
wget https://gitlab.com/bioinformatics-classes/linux-shell-101/raw/master/assets/file.tar.xz
# Uncompress all these files
# Concatenate the contents of all resulting files in a single file and compress it
```

---

### References

* [What is the linux shell](https://bash.cyberciti.biz/guide/What_is_Linux_Shell)
* [The $PATH variable](https://linuxconfig.org/linux-path-environment-variable)
* [File permissions in Linux](http://www.freeos.com/articles/3127/)
* [Stdin and Stdout](http://www.learnlinux.org.za/courses/build/shell-scripting/ch01s04.html)
* [`tar` manpage](https://linux.die.net/man/1/tar)
* [Bashcrawl](https://gitlab.com/slackermedia/bashcrawl)
